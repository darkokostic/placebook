**IMPORTANT GLOBAL CONFIGURATION**

```
#!html
1. Installed Node.js
2. npm install -g cordova(for global)
3. npm install -g ionic(for global)

```

**AFTER PULL**
```
#!html
1. npm install
2. bower install
```

**IF TESTING ON BROWSER:**
```
#!html
1. ionic serve
```

**IF TESTING ON ANDROID**
```
#!html
1. ionic cordova run android (ionic cordova emulate android for emulator)
```

**IF TESTING ON IOS**
```
#!html
1. ionic cordova platform add ios
2. ionic cordova build ios
3. ionic cordova run ios (ionic emulate ios for emulator)
```