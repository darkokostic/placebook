angular.module('placebook', ['ionic', 'ngCordova', 'ngStorage', 'pascalprecht.translate', 'placebook.controllers', 'placebook.services', 'ngMap'])

    .run(function ($ionicPlatform, $ionicConfig, $cordovaNetwork, $translate, $state, $ionicPopup, $rootScope, $cordovaGeolocation, $localStorage, $interval, HttpGetService) {

        if ($localStorage.language == undefined) {
            var lang = window.navigator.language;
            lang = lang.substring(0, 2);
            $translate.use(lang);
            $localStorage.language = lang;
        } else {
            $translate.use($localStorage.language);
        }

        if ($localStorage.units == undefined) {
            $localStorage.units = 'metric';
        }

        $localStorage.location = "current";

        $rootScope.headerSearch = "button-light";
        $rootScope.changeLocationIcon = "img/hamburger.png";

        $rootScope.$on('$stateChangeSuccess', function () {
            if ($state.current.name == "search") {
                $rootScope.headerSearch = "header-active";
            } else {
                $rootScope.headerSearch = "button-light";
            }
        });

        var internetConnected = true;
        var alertPopup = null;
        var alertLocationPopup = null;

        // DISABLE HARDWARE BACK BUTTON ON POPUP
        $ionicPlatform.onHardwareBackButton(function () {
            if (alertPopup != null) {
                alertPopup.close();
                navigator.app.exitApp();
            }

            if (alertLocationPopup != null) {
                alertLocationPopup.close();
                navigator.app.exitApp();
            }
        }, 100);

        $ionicPlatform.ready(function () {
            setTimeout(function () {
                navigator.splashscreen.hide();
            }, 500);

            // CHECK GPS IS ENABLED
            cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
                    if (enabled) {
                        getAds();
                    } else {
                        function locationAlert() {
                            alertLocationPopup = $ionicPopup.alert({
                                title: 'GPS',
                                templateUrl: 'templates/popups/gps.html'
                            });

                            alertLocationPopup.then(function (res) {
                                if (res) {
                                    cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
                                        if (enabled) {
                                            getAds();
                                            alertLocationPopup.close();
                                        } else {
                                            locationAlert();
                                        }
                                    });
                                }
                            });
                        };

                        locationAlert();
                    }
                },
                function (error) {
                    console.error("The following error occurred: " + error);
                });


            var getAds = function () {
                $cordovaGeolocation
                    .getCurrentPosition({enableHighAccuracy: true})
                    .then(function (position) {
                        var latitude = position.coords.latitude;
                        var longitude = position.coords.longitude;

                        HttpGetService.getFirstAd(latitude, longitude)
                            .then(function (data) {
                                if (data != "Error") {
                                    $rootScope.ad = data.entity;
                                }

                            })

                        $rootScope.$on('$stateChangeSuccess', function () {
                            $interval.cancel($rootScope.interval);

                            if ($state.current.name != "categories") {
                                if ($rootScope.ad != undefined || $rootScope.ad != null) {
                                    $rootScope.interval = $interval(changeAd, ($rootScope.ad.timeout * 1000));
                                } else {
                                    HttpGetService.getFirstAd(latitude, longitude)
                                        .then(function (data) {
                                            if (data != "Error") {
                                                $rootScope.ad = data.entity;
                                                $rootScope.interval = $interval(changeAd, ($rootScope.ad.timeout * 1000));
                                            }
                                        })
                                }

                                function changeAd() {
                                    HttpGetService.getRandomAd(latitude, longitude, $rootScope.ad.name)
                                        .then(function (data) {
                                            if (data != "Error") {
                                                $rootScope.ad = data.entity;
                                            }
                                        })
                                }
                            }
                        });
                    }, function (err) {
                        // error
                    });
            }

            if (window.Connection) {

                // CHECK INTERNTET CONNECTION
                $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
                    if (!internetConnected) return;
                    internetConnected = false;

                    function networkAlert() {
                        alertPopup = $ionicPopup.alert({
                            title: 'Internet konekcija',
                            templateUrl: 'templates/popups/network.html',
                            buttons: null
                        });

                        alertPopup.then(function (res) {
                            if (res) {
                                if ($cordovaNetwork.isOnline() == false) {
                                    networkAlert();
                                } else {
                                    alertPopup.close();
                                }
                            }
                        });
                    };

                    networkAlert();

                });

                $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                    if (internetConnected) return;
                    internetConnected = true;
                    if (alertPopup != null) {
                        alertPopup.close();
                        alertPopup = null;
                    }
                });
            }

            // REMOVE BACK BUTTON TEXT
            $ionicConfig.backButton.text("");

            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });

    })

    .config(function ($stateProvider, $urlRouterProvider, $translateProvider) {

        //Translate
        $translateProvider.translations('en', {
            'BANKS': 'BANK',
            'AUTO_SERVICE': 'CAR SERVICE',
            'FINANCE': 'EXCHANGE OFFICE',
            'WEATHER': 'WEATHER',
            'RENT_A_CAR': 'RENT A CAR',
            'TAXI': 'TAXI',
            'SHOPPING': 'SHOPPING',
            'COFFE_SHOPS': 'CAFFE BAR',
            'GAS_STATIONS': 'GAS STATION',
            'HOSPITALS': 'HOSPITAL',
            'HOTELS': 'HOTEL',
            'CINEMA': 'CINEMA',
            'PARKING': 'PARKING',
            'PHARMACIES': 'PHARMACY',
            'NIGHT_CLUBS': 'NIGHT CLUB',
            'RESTAURANTS': 'RESTAURANT',
            'SUPERMARKETS': 'SUPERMARKET',
            'THEATERS': 'THEATER',
            'SEARCH': 'Search',
            'GPS_MESSAGE': 'Turn on your GPS',
            'NETTWORK_MESSAGE': 'No internet connection',
            'PHONE_MESSAGE': 'No phone number',
            'EMPTY_SEARCH': 'No results',
            'SETTINGS': 'SETTINGS',
            'CHANGE_LOCATION': 'CHANGE LOCATION',
            'CURRENT_LOCATION': 'Current location',
            'CHANGE_LANGUAGE': 'CHOOSE LANGUAGE',
            'CHANGE_FORMAT': 'CHOOSE UNITS',
            'DENTIST': 'DENTIST',
            'HISTORIC': 'TOURIST ATTRACTIONS'
        });

        $translateProvider.translations('fr', {
            'BANKS': 'la banque',
            'AUTO_SERVICE': 'service automobile',
            'FINANCE': 'le bureau de change',
            'WEATHER': 'le temps',
            'RENT_A_CAR': 'rent a car',
            'TAXI': 'le taxi',
            'SHOPPING': 'le shopping',
            'COFFE_SHOPS': 'le café',
            'GAS_STATIONS': 'le station de gaz',
            'HOSPITALS': "l'hôpital",
            'HOTELS': "l'hôtel",
            'CINEMA': 'le cinéma',
            'PARKING': 'le parking',
            'PHARMACIES': 'la pharmacie',
            'NIGHT_CLUBS': 'la discothèque',
            'RESTAURANTS': 'le restaurant',
            'SUPERMARKETS': 'le supermarché',
            'THEATERS': 'le théâtre',
            'SEARCH': 'Recherche',
            'GPS_MESSAGE': 'Démarre GPS',
            'PHONE_MESSAGE': "Il n'y a pas le numéro de téléphone",
            'NETTWORK_MESSAGE': "Vous n'avez pas internet",
            'EMPTY_SEARCH': "Il n'y a pas de résultat",
            'SETTINGS': 'LE RÉGLAGE',
            'CHANGE_LOCATION': 'CHANGE LA LOCATION',
            'CURRENT_LOCATION': "L'emplacement actuel",
            'CHANGE_LANGUAGE': 'CHOISIR LA LANGUE',
            'CHANGE_FORMAT': 'CHOISIR FORMAT',
            'DENTIST': 'DENTISTE',
            'HISTORIC': 'ATTRACTIONS TOURISTIQUES'
        });

        $translateProvider.translations('tr', {
            'BANKS': 'BANKA',
            'AUTO_SERVICE': 'OTO SERVIS',
            'FINANCE': 'DÖVIZ BÜROSU',
            'WEATHER': 'HAVA',
            'RENT_A_CAR': 'ARAÇ KIRALAMA',
            'TAXI': 'TAKSI',
            'SHOPPING': 'ALIŞVERIŞ',
            'COFFE_SHOPS': 'KAFETERYA',
            'GAS_STATIONS': 'BENZIN ISTASYONU',
            'HOSPITALS': 'HASTANE',
            'HOTELS': 'OTEL',
            'CINEMA': 'SINEMA',
            'PARKING': 'OTOPARK',
            'PHARMACIES': 'ECZANE',
            'NIGHT_CLUBS': 'GECE KLÜBÜ',
            'RESTAURANTS': 'RESTORAN',
            'SUPERMARKETS': 'SÜPERMARKET',
            'THEATERS': 'TIYATRO',
            'SEARCH': 'Arama',
            'GPS_MESSAGE': 'GPS’ yi aç',
            'PHONE_MESSAGE': 'Telefon numarası yok',
            'NETTWORK_MESSAGE': 'Internet bağlantısı yok',
            'EMPTY_SEARCH': 'Sonuç yok',
            'SETTINGS': 'AYARLAR',
            'CHANGE_LOCATION': 'YER DEĞIŞTIRMEK',
            'CURRENT_LOCATION': 'Mevcut konum',
            'CHANGE_LANGUAGE': 'DIL SEÇINIZ',
            'CHANGE_FORMAT': 'BİRİMLERİ SEÇİN',
            'DENTIST': 'DİŞ DOKTORU',
            'HISTORIC': 'ATRAKSİYONLARI'
        });

        $translateProvider.translations('de', {
            'BANKS': 'BANK',
            'AUTO_SERVICE': 'AUTOWERKSTATT',
            'FINANCE': 'WECHSELSTUBE',
            'WEATHER': 'WETTER',
            'RENT_A_CAR': 'AUTOVERMIETUNG',
            'TAXI': 'TAXI',
            'SHOPPING': 'EINKAUFEN',
            'COFFE_SHOPS': 'KAFFEEBAR',
            'GAS_STATIONS': 'TANKSTELLE',
            'HOSPITALS': 'KRANKENHAUS',
            'HOTELS': 'HOTEL',
            'CINEMA': 'KINO',
            'PARKING': 'PARKPLATZ',
            'PHARMACIES': 'APOTHEKE',
            'NIGHT_CLUBS': 'NACHTCLUB',
            'RESTAURANTS': 'RESTAURANT',
            'SUPERMARKETS': 'SUPERMARKT',
            'THEATERS': 'THEATER',
            'SEARCH': 'Suchen',
            'GPS_MESSAGE': 'GPS Einschalten',
            'PHONE_MESSAGE': 'Nummer nicht vergeben',
            'NETTWORK_MESSAGE': 'Kein Internet',
            'EMPTY_SEARCH': 'Kein ergebnis',
            'SETTINGS': 'EINSTELLUNGEN',
            'CHANGE_LOCATION': 'STANDORT ÄNDERN ',
            'CURRENT_LOCATION': 'Ihr standort',
            'CHANGE_LANGUAGE': 'SPRACHE AUSWÄHLEN',
            'CHANGE_FORMAT': 'WÄHLE EINHEITEN',
            'DENTIST': 'ZAHNARZTPRAXIS',
            'HISTORIC': 'SEHENSWÜRDIGKEITEN'
        });

        $translateProvider.translations('it', {
            'BANKS': 'BANCA',
            'AUTO_SERVICE': 'STAZIONE DI SERVIZIO',
            'FINANCE': 'CAMBIAMONETE',
            'WEATHER': 'PREVISIONE',
            'RENT_A_CAR': 'MACCHINA IN NALEGGIO',
            'TAXI': 'TAXI',
            'SHOPPING': 'SHOPPING',
            'COFFE_SHOPS': 'CAFFE BAR',
            'GAS_STATIONS': 'BENZINAIO',
            'HOSPITALS': 'OSPEDALE',
            'HOTELS': 'ALBERGO',
            'CINEMA': 'CINEMA',
            'PARKING': 'PARCHEGGIO',
            'PHARMACIES': 'FARMACIA',
            'NIGHT_CLUBS': 'NIGHT CLUB',
            'RESTAURANTS': 'RISTORANTE',
            'SUPERMARKETS': 'SUPERMERCATO',
            'THEATERS': 'TEATRO',
            'SEARCH': 'Cerca',
            'GPS_MESSAGE': 'GPS non accesso',
            'PHONE_MESSAGE': "Non c'e` numero di telefono",
            'NETTWORK_MESSAGE': 'Nessun accesso internet',
            'EMPTY_SEARCH': 'Nessun risultato',
            'SETTINGS': 'IMPOSTAZIONI',
            'CHANGE_LOCATION': 'CAMBIARE LOCAZIONE',
            'CURRENT_LOCATION': 'Posizione',
            'CHANGE_LANGUAGE': 'SCEGLIERE LINGUA',
            'CHANGE_FORMAT': 'SCEGLIERE LE UNITÀ',
            'DENTIST': 'DENTISTA',
            'HISTORIC': 'ATTRAZIONI TURISTICHE'
        });

        $translateProvider.translations('sr', {
            'BANKS': 'BANKA',
            'AUTO_SERVICE': 'AUTO SERVIS',
            'FINANCE': 'MENJAČNICA',
            'WEATHER': 'VREME',
            'RENT_A_CAR': 'RENT A CAR',
            'TAXI': 'TAKSI',
            'SHOPPING': 'PRODAVNICA',
            'COFFE_SHOPS': 'KAFIĆ',
            'GAS_STATIONS': 'PUMPA',
            'HOSPITALS': 'BOLNICA',
            'HOTELS': 'HOTEL',
            'CINEMA': 'BIOSKOP',
            'PARKING': 'PARKING',
            'PHARMACIES': 'APOTEKA',
            'NIGHT_CLUBS': 'NOĆNI KLUB',
            'RESTAURANTS': 'RESTORAN',
            'SUPERMARKETS': 'SUPERMARKET',
            'THEATERS': 'POZORIŠTE',
            'SEARCH': 'Pretraži',
            'GPS_MESSAGE': 'Uključite GPS na vašem uređaju',
            'PHONE_MESSAGE': 'Nema broja telefona',
            'NETTWORK_MESSAGE': 'Nemate internet konekciju',
            'EMPTY_SEARCH': 'Nema rezultata',
            'SETTINGS': 'PODEŠAVANJA',
            'CHANGE_LOCATION': 'PROMENI LOKACIJU',
            'CURRENT_LOCATION': 'Trenutna lokacija',
            'CHANGE_LANGUAGE': 'IZABERITE JEZIK',
            'CHANGE_FORMAT': 'IZABERITE FORMAT',
            'DENTIST': 'ZUBAR',
            'HISTORIC': 'ZNAMENITOSTI'
        });

        $translateProvider.translations('sl', {
            'BANKS': 'BANKA',
            'AUTO_SERVICE': 'AVTO SERVIS',
            'FINANCE': 'MENJALNICA',
            'WEATHER': 'VREME',
            'RENT_A_CAR': 'RENT A CAR',
            'TAXI': 'TAXI',
            'SHOPPING': 'TRGOVINA',
            'COFFE_SHOPS': 'KAVARNA',
            'GAS_STATIONS': 'BENCINSKA ČRPALKA',
            'HOSPITALS': 'BOLNIŠNICA',
            'HOTELS': 'HOTEL',
            'CINEMA': 'KINO',
            'PARKING': 'PARKIRIŠČE',
            'PHARMACIES': 'LEKARNA',
            'NIGHT_CLUBS': 'NOČNI KLUB',
            'RESTAURANTS': 'GOSTILNA',
            'SUPERMARKETS': 'SUPERMARKET',
            'THEATERS': 'GLEDALIŠČE',
            'SEARCH': 'Išči',
            'GPS_MESSAGE': 'Uklopite gps',
            'PHONE_MESSAGE': 'Ne obstaja telefonska številka ',
            'NETTWORK_MESSAGE': 'Nedosegljiv internet',
            'EMPTY_SEARCH': ' Nima rezultata',
            'SETTINGS': 'NASTAVITVE',
            'CHANGE_LOCATION': 'SPREMEMBA LOKACIJA',
            'CURRENT_LOCATION': 'Trenutna lokacija',
            'CHANGE_LANGUAGE': 'IZBERITE JEZIK',
            'CHANGE_FORMAT': 'IZBERITE FORMAT',
            'DENTIST': 'ZOBOZDRAVNIK',
            'HISTORIC': 'ZNAMENITOSTI'
        });

        $translateProvider.translations('sv', {
            'BANKS': 'BANK',
            'AUTO_SERVICE': 'BILVERKSTAD',
            'FINANCE': 'VÄXLINGSKONTOR',
            'WEATHER': 'VÄDER',
            'RENT_A_CAR': 'BILUTHYRNING',
            'TAXI': 'TAXI',
            'SHOPPING': 'SHOPPING',
            'COFFE_SHOPS': 'CAFÉ',
            'GAS_STATIONS': 'BENSINSTATION',
            'HOSPITALS': 'SJUKHUS',
            'HOTELS': 'HOTELL',
            'CINEMA': 'BIOGRAF',
            'PARKING': 'PARKERING',
            'PHARMACIES': 'APOTEK',
            'NIGHT_CLUBS': 'NATT KLUBB',
            'RESTAURANTS': 'RESTAURANG',
            'SUPERMARKETS': 'LIVSMEDELSBUTIK',
            'THEATERS': 'TEATER',
            'SEARCH': 'Sök',
            'GPS_MESSAGE': 'Sätt på GPS',
            'NETTWORK_MESSAGE': 'Ingen nätverk',
            'PHONE_MESSAGE': 'Telefonnummer saknas',
            'EMPTY_SEARCH': 'Sökresultat saknas',
            'SETTINGS': 'INSTÄLLNINGAR',
            'CHANGE_LOCATION': 'BYT PLATS',
            'CURRENT_LOCATION': 'Nuvarande position',
            'CHANGE_LANGUAGE': 'VÄLJ SPRÅK',
            'CHANGE_FORMAT': 'VÄLJ ENHETER',
            'DENTIST': 'TANDLÄKARE',
            'HISTORIC': 'SEVÄRDHETER'
        });

        $translateProvider.translations('el', {
            'BANKS': 'TRAPEZA',
            'AUTO_SERVICE': 'SINERGIO AFTOKINITON',
            'FINANCE': 'ADALAKTIRIO',
            'WEATHER': 'KEROS',
            'RENT_A_CAR': 'ENIKIASIS AFTOKINITON',
            'TAXI': 'TAXI',
            'SHOPPING': 'PSONIA',
            'COFFE_SHOPS': 'KAFETERIA',
            'GAS_STATIONS': 'VENZINADIKO',
            'HOSPITALS': 'NOSOKOMIO',
            'HOTELS': 'KSENODOXIO',
            'CINEMA': 'KINIMATOGRAFOS',
            'PARKING': 'PARKING',
            'PHARMACIES': 'FARMAKIO',
            'NIGHT_CLUBS': 'KLAB',
            'RESTAURANTS': 'ESTIATORIO',
            'SUPERMARKETS': 'SUPERMARKET',
            'THEATERS': 'THEATRO',
            'SEARCH': 'Anazitisi',
            'GPS_MESSAGE': 'Energopiiste to GPS',
            'PHONE_MESSAGE': 'Den iparxei arithmos tilefonou',
            'NETTWORK_MESSAGE': 'Den exete internet',
            'EMPTY_SEARCH': 'Den iparxei apotelesma',
            'SETTINGS': 'RITHMISIS',
            'CHANGE_LOCATION': 'ALAKSE TOPOTHESIA',
            'CURRENT_LOCATION': 'Τrexusa thesi',
            'CHANGE_LANGUAGE': 'EPILEKSTE GLOSSA',
            'CHANGE_FORMAT': 'EPILEGOUN MONADES',
            'DENTIST': 'ODONTIATRIO',
            'HISTORIC': 'AKSIOTHEATA'
        });

        $translateProvider.translations('cn', {
            'BANKS': '银行',
            'AUTO_SERVICE': '汽车服务',
            'FINANCE': '货币兑换',
            'WEATHER': '天气',
            'RENT_A_CAR': '汽车出租',
            'TAXI': '出租车',
            'SHOPPING': '商店',
            'COFFE_SHOPS': '咖啡店',
            'GAS_STATIONS': '加油站',
            'HOSPITALS': '医院',
            'HOTELS': '饭店',
            'CINEMA': '电影院',
            'PARKING': '停車處',
            'PHARMACIES': '药店',
            'NIGHT_CLUBS': '夜总会',
            'RESTAURANTS': '餐馆',
            'SUPERMARKETS': '超级市场',
            'THEATERS': '剧院',
            'SEARCH': '搜索',
            'GPS_MESSAGE': '打开gps',
            'PHONE_MESSAGE': '该号码不存在',
            'NETTWORK_MESSAGE': '没有网络连接',
            'EMPTY_SEARCH': '未找到结果',
            'SETTINGS': '设置',
            'CHANGE_LOCATION': '更改位置',
            'CURRENT_LOCATION': '当前位置',
            'CHANGE_LANGUAGE': 'CHANGE FORMAT',
            'CHANGE_FORMAT': '选择语言',
            'DENTIST': '牙科中心',
            'HISTORIC': '旅游景点'
        });

        $translateProvider.translations('hu', {
            'BANKS': 'BANK',
            'AUTO_SERVICE': 'AUTÓSZERVÍZ',
            'FINANCE': 'PÉNZVÁLTÓ',
            'WEATHER': 'IDŐJÁRÁS',
            'RENT_A_CAR': 'AUTÓBÉRLÉS',
            'TAXI': 'TAXI',
            'SHOPPING': 'ÜZLET',
            'COFFE_SHOPS': 'KÁVÉZÓ',
            'GAS_STATIONS': 'BENZINKÚT',
            'HOSPITALS': 'KÓRHÁZ',
            'HOTELS': 'SZÁLLODA',
            'CINEMA': 'MOZI',
            'PARKING': 'PARKOLÓ',
            'PHARMACIES': 'GYÓGYSZERTÁR',
            'NIGHT_CLUBS': 'SZÓRAKOZÓHELY',
            'RESTAURANTS': 'ÉTTEREM',
            'SUPERMARKETS': 'BEVÁSÁRLÓKÖZPONT',
            'THEATERS': 'SZÍNHÁZ',
            'SEARCH': 'Keresés',
            'GPS_MESSAGE': 'Kapcsolja be a gps',
            'PHONE_MESSAGE': 'A telefonszám nem létezik',
            'NETTWORK_MESSAGE': 'Hincs internet',
            'EMPTY_SEARCH': 'Nincs eredmény',
            'SETTINGS': 'BEÁLLÍTÁSOK',
            'CHANGE_LOCATION': 'A HELY MÓDOSÍTÁSA',
            'CURRENT_LOCATION': 'Jelenlegi hely',
            'CHANGE_LANGUAGE': 'VÁLASSZON NYELVET',
            'CHANGE_FORMAT': 'CHANGE FORMAT',
            'DENTIST': 'Fogászati központ',
            'HISTORIC': 'Turistalátványosság'
        });

        $translateProvider.preferredLanguage('en');

        $stateProvider

            .state('categories', {
                url: '/categories',
                templateUrl: 'templates/categories.html',
                controller: 'CategoriesCtrl'
            })

            .state('weather', {
                url: '/categories/weather',
                templateUrl: 'templates/weather.html',
                controller: 'WeatherCtrl'
            })

            .state('search', {
                url: '/search',
                templateUrl: 'templates/search.html',
                controller: 'SearchCtrl'
            })

            .state('changelocation', {
                url: '/categories/change-location',
                templateUrl: 'templates/change-location.html',
                controller: 'ChangeLocationCtrl'
            })

            .state('settings', {
                url: '/categories/settings',
                templateUrl: 'templates/settings.html',
                controller: 'SettingsCtrl'
            })

            .state('singlecat', {
                url: '/categories/:catId',
                params: {
                    catId: null
                },
                templateUrl: 'templates/category-single.html',
                controller: 'CategorySingleCtrl'
            })

            .state('map', {
                url: '/map',
                params: {
                    latitude: null,
                    longitude: null,
                    vicinity: null,
                    placeId: null,
                    distance: null
                },
                templateUrl: 'templates/map.html',
                controller: 'MapCtrl'
            })

            .state('sponsoredmap', {
                url: '/sponsored-map',
                params: {
                    sponsoredName: null,
                    sponsoredLat: null,
                    sponsoredLng: null,
                    km: null,
                    meters: null
                },
                templateUrl: 'templates/sponsored-map.html',
                controller: 'SponsoredMapCtrl'
            });

        $urlRouterProvider.otherwise('/categories');

    });