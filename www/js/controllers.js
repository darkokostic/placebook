angular.module('placebook.controllers', [])

.controller('AppCtrl', function($scope, $state, $rootScope, $ionicPopup, $ionicPopover, HttpGetService, $cordovaGeolocation) {

	$ionicPopover.fromTemplateUrl('templates/popups/popover.html', {
		scope: $scope,
	}).then(function(popover) {
		$scope.popover = popover;
		$scope.$on('popover.hidden', function() {
		    $rootScope.headerHamburger = "button-light";
		});
	});

	$scope.changeIcon = function() {
		$rootScope.headerHamburger = "header-active";
	}

	$scope.menuGo = function(state) {
		// CHECK GPS IS ENABLED
	    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	        if(enabled) {
	            $state.go(state);
	        } else {
	            function locationAlert() {
	                var alertLocationPopup = $ionicPopup.alert({
	                    title: 'GPS',
	                    templateUrl: 'templates/popups/gps.html'
	                });
	               
	                alertLocationPopup.then(function(res) {
	                    if(res) {
	                        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	                            if(enabled) {
	                              alertLocationPopup.close();
	                            } else {
	                              locationAlert();
	                            }
	                        });
	                    }
	                });
	           };

	           locationAlert();   
	        }
	    }, 
	      function(error){
	        console.error("The following error occurred: "+error);
	    });
	}

	$scope.searchGo = function() {
		// CHECK GPS IS ENABLED
	    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	        if(enabled) {
	            $state.go('search');
	        } else {
	            function locationAlert() {
	                var alertLocationPopup = $ionicPopup.alert({
	                    title: 'GPS',
	                    templateUrl: 'templates/popups/gps.html'
	                });
	               
	                alertLocationPopup.then(function(res) {
	                    if(res) {
	                        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	                            if(enabled) {
	                              alertLocationPopup.close();
	                            } else {
	                              locationAlert();
	                            }
	                        });
	                    }
	                });
	           };

	           locationAlert();   
	        }
	    }, 
	      function(error){
	        console.error("The following error occurred: "+error);
	    });
	}
})

.controller('SearchCtrl', function($scope, $state, $ionicNavBarDelegate, $timeout, $ionicLoading, $interval, $cordovaInAppBrowser, $rootScope, $cordovaGeolocation, GetPlacesService, $localStorage) {

	$ionicNavBarDelegate.showBackButton(true);
	$scope.$on('$ionicView.enter', function()
	{
		$timeout(function()
		{
			$ionicNavBarDelegate.align('center');
		});
	});

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

	$scope.empty = false;

	$scope.search = function(keyword) {
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$scope.searchedPlaces = null;

		if($localStorage.location == "current") {
			$cordovaGeolocation
				.getCurrentPosition({enableHighAccuracy: true})
				.then(function(position) {

					var lat  = position.coords.latitude;
					var long = position.coords.longitude;

					var location = lat+','+long;

					GetPlacesService.getPlaces(location, '', keyword)
						.then(function(data) {

							GetPlacesService.getDistance(data, lat, long);

							for(var i = 0; i < data.length; i++) {
								GetPlacesService.getInfo(data, i);
							}

							$scope.searchedPlaces = data;

							if($scope.searchedPlaces.length != 0) {
								$ionicLoading.hide();
								$scope.empty = false;
							} else {
								$ionicLoading.hide();
								$scope.empty = true;
							}

						}, function(err) {
							// error
						});
				});
		} else {
			var location = $localStorage.location;

			GetPlacesService.getPlaces(location, '', keyword)
				.then(function(data) {

					for(var i = 0; i < data.length; i++) {
						GetPlacesService.getDistance(data, i, location);
					}

					$scope.searchedPlaces = data;

					if($scope.searchedPlaces.length != 0) {
						$ionicLoading.hide();
						$scope.empty = false;
					} else {
						$ionicLoading.hide();
						$scope.empty = true;
					}

				}, function(err) {
					// error
				});
		}
	}

	$scope.goMap = function(latitude, longitude, vicinity, placeId, distance) {
		$ionicLoading.show({
			content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		$cordovaGeolocation
			.getCurrentPosition({enableHighAccuracy: true})
			.then(function (position) {

				var currLatitude  = position.coords.latitude;
				var currLongitude = position.coords.longitude;

				$rootScope.startLatitude = currLatitude;
				$rootScope.startLongitude = currLongitude;

				var latlng = currLatitude+','+currLongitude;

				$rootScope.currentAddres = latlng;

				$ionicLoading.hide();
				$state.go('map', {latitude: latitude, longitude:longitude, vicinity:vicinity, placeId:placeId, distance:distance});

			}, function(err) {
				// error
			});
	}
})

.controller('CategoriesCtrl', function($scope, $rootScope, $timeout, $ionicHistory, $ionicNavBarDelegate, $state, $ionicLoading, $cordovaGeolocation, GetPlacesService, $http, $q, $ionicPopup, $localStorage, WeatherService, HttpGetService) {
	
	$ionicNavBarDelegate.showBackButton(false);

	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('left');
	    });
	});

	$ionicHistory.clearHistory();

	$scope.grid0 = "grid-item";
	$scope.grid1 = "grid-item";
	$scope.grid2 = "grid-item";
	$scope.grid3 = "grid-item";
	$scope.grid4 = "grid-item";
	$scope.grid5 = "grid-item";
	$scope.grid6 = "grid-item";
	$scope.grid7 = "grid-item";
	$scope.grid8 = "grid-item";
	$scope.grid9 = "grid-item";
	$scope.grid10 = "grid-item";
	$scope.grid11 = "grid-item";
	$scope.grid12 = "grid-item";
	$scope.grid13 = "grid-item";
	$scope.grid14 = "grid-item";
	$scope.grid15 = "grid-item";
	$scope.grid16 = "grid-item";
	$scope.grid17 = "grid-item";
    $scope.grid18 = "grid-item";
    $scope.grid19 = "grid-item";

	$scope.requestPlaces = function(type, keyword, id, categorie) {

        checkPermission();

		function checkPermission() {
            cordova.plugins.permissions.checkPermission(cordova.plugins.permissions.ACCESS_COARSE_LOCATION,
                function(response) {
                    if(response.hasPermission) {
                    	checkLocation();
					} else {
                        function locationAlert() {
                            var alertLocationPopup = $ionicPopup.alert({
                                title: 'GPS',
                                templateUrl: 'templates/popups/gps.html'
                            });

                            alertLocationPopup.then(function(res) {
                                console.log(res);
                            });
                        };

                        locationAlert();
					}
                });
		}


        function checkLocation() {
            // CHECK GPS IS ENABLED
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                    if(enabled) {
                        go();
                    } else {
                        function locationAlert() {
                            var alertLocationPopup = $ionicPopup.alert({
                                title: 'GPS',
                                templateUrl: 'templates/popups/gps.html'
                            });

                            alertLocationPopup.then(function(res) {
                                if(res) {
                                    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                                        if(enabled) {
                                            alertLocationPopup.close();
                                        } else {
                                            locationAlert();
                                        }
                                    });
                                }
                            });
                        };

                        locationAlert();
                    }
                },
                function(error){
                    console.error("The following error occurred: "+error);
                });
		}

	    var go = function() {

	    	switch(id) {
				case 0:
					$scope.grid0 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/restoran.png";
					break;
				case 1:
					$scope.grid1 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/apoteka.png";
					break;
				case 2:
					$scope.grid2 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/cafe.png";
					break;
				case 3:
					$scope.grid3 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/bolnica.png";
					break;
				case 4:
					$scope.grid4 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/pozoriste.png";
					break;
				case 5:
					$scope.grid5 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/auto-servis.png";
					break;
				case 6:
					$scope.grid6 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/hotel.png";
					break;
				case 7:
					$scope.grid7 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/menjacnica.png";
					break;
				case 8:
					$scope.grid8 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/bank.png";
					break;
				case 9:
					$scope.grid9 = "grid-item grid-item-dark";
					break;
				case 10:
					$scope.grid10 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/pumpa.png";
					break;
				case 11:
					$scope.grid11 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/parking.png";
					break;
				case 12:
					$scope.grid12 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/rent-a-car.png";
					break;
				case 13:
					$scope.grid13 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/supermarket.png";
					break;
				case 14:
					$scope.grid14 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/bar.png";
					break;
				case 15:
					$scope.grid15 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/bioskop.png";
					break;
				case 16:
					$scope.grid16 = "grid-item grid-item-dark grid-middle-selected";
					$rootScope.categorieImage = "img/taxi.png";
					break;
				case 17:
					$scope.grid17 = "grid-item grid-item-dark";
					$rootScope.categorieImage = "img/shopping.png";
					break;
                case 18:
                    $scope.grid18 = "grid-item grid-item-dark";
                    $rootScope.categorieImage = "img/shopping.png";
                    break;
                case 19:
                    $scope.grid19 = "grid-item grid-item-dark";
                    $rootScope.categorieImage = "img/shopping.png";
                    break;
			}

	    	$ionicLoading.show({
	            content: 'Loading',
	            animation: 'fade-in',
	            showBackdrop: true,
	            maxWidth: 200,
	            showDelay: 0
	        });

			$rootScope.places = null;

			if($localStorage.location == "current") {

				$cordovaGeolocation
			    .getCurrentPosition({enableHighAccuracy: true})
			    .then(function(position) {
			      	var lat  = position.coords.latitude;
			      	var long = position.coords.longitude;

			      	var location = lat+','+long;

			      	HttpGetService.getSponsoredList(categorie, lat, long)
		            .then(function(data) {

                        GetPlacesService.getPlaces(location, type, keyword)
                            .then(function(data) {

                                GetPlacesService.getDistance(data, lat, long);

                                var counter = data.length;

                                if(counter > 10) {
                                    var i = 0;
                                    for(i; i < 10; i++) {
                                        GetPlacesService.getInfo(data, i);
                                    }
                                    counter = 10;
                                } else {
                                    var i = 0;
                                    for(i; i < data.length; i++) {
                                        GetPlacesService.getInfo(data, i);
                                    }
                                }

                                if(i == counter) {

                                    $rootScope.places = data;
                                    $rootScope.currentLocation = location;

                                    if($rootScope.places != null) {

                                        $ionicLoading.hide();

                                        $scope.grid0 = "grid-item";
                                        $scope.grid1 = "grid-item";
                                        $scope.grid2 = "grid-item";
                                        $scope.grid3 = "grid-item";
                                        $scope.grid4 = "grid-item";
                                        $scope.grid5 = "grid-item";
                                        $scope.grid6 = "grid-item";
                                        $scope.grid7 = "grid-item";
                                        $scope.grid8 = "grid-item";
                                        $scope.grid9 = "grid-item";
                                        $scope.grid10 = "grid-item";
                                        $scope.grid11 = "grid-item";
                                        $scope.grid12 = "grid-item";
                                        $scope.grid13 = "grid-item";
                                        $scope.grid14 = "grid-item";
                                        $scope.grid15 = "grid-item";
                                        $scope.grid16 = "grid-item";
                                        $scope.grid17 = "grid-item";
                                        $scope.grid18 = "grid-item";
                                        $scope.grid19 = "grid-item";
                                        $state.go('singlecat', {catId:id});

                                    } else {

                                        $ionicLoading.hide();

                                        $scope.grid0 = "grid-item";
                                        $scope.grid1 = "grid-item";
                                        $scope.grid2 = "grid-item";
                                        $scope.grid3 = "grid-item";
                                        $scope.grid4 = "grid-item";
                                        $scope.grid5 = "grid-item";
                                        $scope.grid6 = "grid-item";
                                        $scope.grid7 = "grid-item";
                                        $scope.grid8 = "grid-item";
                                        $scope.grid9 = "grid-item";
                                        $scope.grid10 = "grid-item";
                                        $scope.grid11 = "grid-item";
                                        $scope.grid12 = "grid-item";
                                        $scope.grid13 = "grid-item";
                                        $scope.grid14 = "grid-item";
                                        $scope.grid15 = "grid-item";
                                        $scope.grid16 = "grid-item";
                                        $scope.grid17 = "grid-item";
                                        $scope.grid18 = "grid-item";
                                        $scope.grid19 = "grid-item";
                                    }
                                }

                            }, function(err) {
                                $ionicLoading.hide();
                            });

		            		if(data != "Error") {
		            			$rootScope.sponsoredPlaces = data.entity;
				    		}
		            })

			    });

			} else {

				var choosedLocation = $localStorage.location;

				HttpGetService.getSponsoredList(categorie, $localStorage.latiduide,	$localStorage.longitude)
		            .then(function(data) {

		            	if(data != "Error") {
							$rootScope.sponsoredPlaces = data.entity;
		            	}

                        GetPlacesService.getPlaces(choosedLocation, type, keyword)
                            .then(function(data) {

                                GetPlacesService.getDistance(data, $localStorage.latiduide,	$localStorage.longitude);

                                var counter = data.length;

                                if(counter > 10) {
                                    var i = 0;
                                    for(i; i < 10; i++) {
                                        GetPlacesService.getInfo(data, i);
                                    }
                                    counter = 10;
                                } else {
                                    var i = 0;
                                    for(i; i < data.length; i++) {
                                        GetPlacesService.getInfo(data, i);
                                    }
                                }

                                if(i == counter) {

                                    $rootScope.places = data;
                                    $rootScope.currentLocation = choosedLocation;

                                    if($rootScope.places != null) {

                                        $ionicLoading.hide();

                                        $scope.grid0 = "grid-item";
                                        $scope.grid1 = "grid-item";
                                        $scope.grid2 = "grid-item";
                                        $scope.grid3 = "grid-item";
                                        $scope.grid4 = "grid-item";
                                        $scope.grid5 = "grid-item";
                                        $scope.grid6 = "grid-item";
                                        $scope.grid7 = "grid-item";
                                        $scope.grid8 = "grid-item";
                                        $scope.grid9 = "grid-item";
                                        $scope.grid10 = "grid-item";
                                        $scope.grid11 = "grid-item";
                                        $scope.grid12 = "grid-item";
                                        $scope.grid13 = "grid-item";
                                        $scope.grid14 = "grid-item";
                                        $scope.grid15 = "grid-item";
                                        $scope.grid16 = "grid-item";
                                        $scope.grid17 = "grid-item";
                                        $state.go('singlecat', {catId:id});

                                    } else {

                                        $ionicLoading.hide();

                                        $scope.grid0 = "grid-item";
                                        $scope.grid1 = "grid-item";
                                        $scope.grid2 = "grid-item";
                                        $scope.grid3 = "grid-item";
                                        $scope.grid4 = "grid-item";
                                        $scope.grid5 = "grid-item";
                                        $scope.grid6 = "grid-item";
                                        $scope.grid7 = "grid-item";
                                        $scope.grid8 = "grid-item";
                                        $scope.grid9 = "grid-item";
                                        $scope.grid10 = "grid-item";
                                        $scope.grid11 = "grid-item";
                                        $scope.grid12 = "grid-item";
                                        $scope.grid13 = "grid-item";
                                        $scope.grid14 = "grid-item";
                                        $scope.grid15 = "grid-item";
                                        $scope.grid16 = "grid-item";
                                        $scope.grid17 = "grid-item";

                                    }
                                }


                            }, function(err) {
                                $ionicLoading.hide();
                            });
		            })

			}
			
	    }
		
	}

	$scope.goWeather = function() {

		// CHECK GPS IS ENABLED
	    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	        if(enabled) {
	            go();
	        } else {
	            function locationAlert() {
	                var alertLocationPopup = $ionicPopup.alert({
	                    title: 'GPS',
	                    templateUrl: 'templates/popups/gps.html'
	                });
	               
	                alertLocationPopup.then(function(res) {
	                    if(res) {
	                        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	                            if(enabled) {
	                              alertLocationPopup.close();
	                            } else {
	                              locationAlert();
	                            }
	                        });
	                    }
	                });
	           };

	           locationAlert();   
	        }
	    }, 
	      function(error){
	        console.error("The following error occurred: "+error);
	    });

		var go = function() {

			$scope.grid9 = "grid-item grid-item-dark";

			$ionicLoading.show({
	            content: 'Loading',
	            animation: 'fade-in',
	            showBackdrop: true,
	            maxWidth: 200,
	            showDelay: 0
	        });

	        switch($localStorage.units) {
	        	case 'metric':
	        		$rootScope.format = 'C';
	        		break;
	        	case 'imperial':
	        		$rootScope.format = 'F';
	        		break;
	        	case '':
	        		$rootScope.format = "K";
	        		break;
	        }

			if($localStorage.location == "current") {

				$cordovaGeolocation
			    .getCurrentPosition({enableHighAccuracy: true})
			    .then(function(position) {

				      	var lat = position.coords.latitude;
				      	var lng = position.coords.longitude;

				      	WeatherService.getWeather(lat, lng)
			            .then(function(data) {

				            	$rootScope.weather = {
				            		name: data.name,
				            		value: data.main.temp.toFixed(),
				            		img: "http://openweathermap.org/img/w/"+ data.weather[0].icon + ".png",
				            	};

				            	WeatherService.getForecast(lat, lng)
					            .then(function(data) {

					            	$rootScope.forecast = [];
					            	for(var i = 0; i < data.length; i++) {
					            		var hasDate = false;
					            		for(var j = 0; j < $rootScope.forecast.length; j++) {
					            			if($rootScope.forecast[j].date && data[i].dt_txt.split(" ")[0] == $rootScope.forecast[j].date) {
                                                hasDate = true;
											}
										}

										if(!hasDate) {
                                            $rootScope.forecast.push({date: data[i].dt_txt.split(" ")[0]});
										}
									}

                                    for(var i = 0; i < $rootScope.forecast.length; i++) {
                                        $rootScope.forecast[i].time = [];
                                        for(var j = 0; j < data.length; j++) {
                                            if(data[j].dt_txt.split(" ")[0] == $rootScope.forecast[i].date) {
                                                $rootScope.forecast[i].time.push(data[j]);
                                            }
										}

										for(var k = 0; k < $rootScope.forecast[i].time.length; k++) {
                                            $rootScope.forecast[i].time[k].time = $rootScope.forecast[i].time[k].dt_txt.split(" ")[1].split(":")[0] + ":" + $rootScope.forecast[i].time[k].dt_txt.split(" ")[1].split(":")[1];
										}
                                    }

					            	$ionicLoading.hide();
					            	$scope.grid9 = "grid-item";
					            	$state.go('weather');
					            	$ionicHistory.nextViewOptions({
							           disableBack: false
							        });

					            })

			            })

			    })

			} else {

				WeatherService.getWeatherChangedLocation($localStorage.myAddress)
	            .then(function(data) {

		            	$rootScope.weather = {
		            		name: data.name,
		            		value: data.main.temp.toFixed(),
		            		img: "http://openweathermap.org/img/w/"+ data.weather[0].icon + ".png",
		            	}
		            	
		            	WeatherService.getForecastChangedLocation($localStorage.myAddress)
			            .then(function(data) {

			            	$rootScope.forecast = data;

			            	$ionicLoading.hide();
			            	$scope.grid9 = "grid-item";
			            	$state.go('weather');
			            	$ionicHistory.nextViewOptions({
					           disableBack: false
					        });

			            })

	            })

			}
			
		}

	}

})

.controller('WeatherCtrl', function($scope, $timeout, $state, $ionicNavBarDelegate, $interval, $rootScope, $cordovaInAppBrowser) {

	$ionicNavBarDelegate.showBackButton(true);
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

})

.controller('SettingsCtrl', function($scope, $rootScope, $localStorage, $interval, $cordovaInAppBrowser, $state, $ionicNavBarDelegate, $ionicHistory, $timeout, $translate) {

	$ionicNavBarDelegate.showBackButton(true);
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.language = $localStorage.language;
	$scope.units = $localStorage.units;

	$scope.changeLang = function(lang) {
		$translate.use(lang);
		$localStorage.language = lang;
	}

	$scope.changeUnits = function(unit) {
		$localStorage.units = unit;
		$state.go('categories');
    	$ionicHistory.nextViewOptions({
           disableBack: false
        });
	}

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

})

.controller('ChangeLocationCtrl', function($scope, $state, $ionicLoading, $rootScope, $ionicNavBarDelegate, $cordovaInAppBrowser, $timeout, $ionicHistory, GetPlacesService, $localStorage, $ionicHistory, $interval) {

	$ionicNavBarDelegate.showBackButton(true);
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

	$scope.search = function(input) {

		$ionicLoading.show({
            content: 'Loading',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

		GetPlacesService.getLocations(input)
        .then(function(data) {

        	$scope.searchedPlaces = data.predictions;
        	$ionicLoading.hide();

	    }, function(err) {
	      // error
	    });

	}

	$scope.setCurrent = function() {
		$localStorage.location = "current";
        $state.go('categories');
        $ionicHistory.nextViewOptions({
           disableBack: true
        });
	}

	$scope.changeLocation = function(placeId, name) {

		GetPlacesService.getPlaceDetails(placeId)
		.then(function(data) {

			$localStorage.location = data.geometry.location.lat+','+data.geometry.location.lng;
			$localStorage.latiduide = data.geometry.location.lat;
			$localStorage.longitude = data.geometry.location.lng;
			$localStorage.myAddress = name;

	        $state.go('categories');
	        $ionicHistory.nextViewOptions({
	           disableBack: true
	        });

		});

	}

})

.controller('CategorySingleCtrl', function($scope, $state, $stateParams, $rootScope, $ionicLoading, $timeout, $ionicNavBarDelegate, $rootScope, $cordovaGeolocation, GetPlacesService, HttpGetService, $ionicPopup, $interval, $cordovaInAppBrowser) {
	
	$ionicNavBarDelegate.showBackButton(true);
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

	$scope.numberOfItemsToDisplay = 10;

	$scope.loadMore = function(counter) {

        if($scope.places.length <= $scope.numberOfItemsToDisplay) {

            $scope.$broadcast('scroll.infiniteScrollComplete');

        } else {

            $timeout(function () {
            	
            	var i = counter;

            	if($scope.places.length - counter >= 10) {

            		for(i; i < (counter + 5); i++) {
		      			GetPlacesService.getInfo($scope.places, i);
		      		}

		      		if(i == (counter + 5)) {
		      			$scope.numberOfItemsToDisplay += 5;
	                	$scope.$broadcast('scroll.infiniteScrollComplete');
		      		}

            	} else {

            		for(i; i < $scope.places.length; i++) {
		      			GetPlacesService.getInfo($scope.places, i);
		      		}

		      		if(i == $scope.places.length) {
		      			$scope.numberOfItemsToDisplay += 5;
	                	$scope.$broadcast('scroll.infiniteScrollComplete');
		      		}

            	}

            }, 2000);
        }        
    }

	$scope.goMap = function(latitude, longitude, vicinity, placeId, distance) {

		// CHECK GPS IS ENABLED
	    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	        if(enabled) {

				   $ionicLoading.show({
			            content: 'Loading',
			            animation: 'fade-in',
			            showBackdrop: true,
			            maxWidth: 200,
			            showDelay: 0
			        });

					$cordovaGeolocation
					.getCurrentPosition({enableHighAccuracy: true})
				    .then(function (position) {

				      	var currLatitude  = position.coords.latitude;
				      	var currLongitude = position.coords.longitude;

				      	$rootScope.startLatitude = currLatitude;
				      	$rootScope.startLongitude = currLongitude;

				      	var latlng = currLatitude+','+currLongitude;

				      	$rootScope.currentAddres = latlng;

				      	$ionicLoading.hide();
				      	$state.go('map', {latitude: latitude, longitude:longitude, vicinity:vicinity, placeId:placeId, distance:distance});


				    }, function(err) {
				      // error
				    });
	            
	        } else {
	            function locationAlert() {
	                var alertLocationPopup = $ionicPopup.alert({
	                    title: 'GPS',
	                    templateUrl: 'templates/popups/gps.html'
	                });
	               
	                alertLocationPopup.then(function(res) {
	                    if(res) {
	                        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	                            if(enabled) {
	                              alertLocationPopup.close();
	                            } else {
	                              locationAlert();
	                            }
	                        });
	                    }
	                });
	           };

	           locationAlert();   
	        }
	    }, 
	      function(error){
	        console.error("The following error occurred: "+error);
	    });

	}

	$scope.goSponsoredMap = function(name, address, km, meters) {

    	// CHECK GPS IS ENABLED
	    cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	        if(enabled) {
	            goSponsored();
	        } else {
	            function locationAlert() {
	                var alertLocationPopup = $ionicPopup.alert({
	                    title: 'GPS',
	                    templateUrl: 'templates/popups/gps.html'
	                });
	               
	                alertLocationPopup.then(function(res) {
	                    if(res) {
	                        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
	                            if(enabled) {
	                              alertLocationPopup.close();
	                            } else {
	                              locationAlert();
	                            }
	                        });
	                    }
	                });
	           };

	           locationAlert();   
	        }
	    }, 
	      function(error){
	        console.error("The following error occurred: "+error);
	    });

    	var goSponsored = function() {

    		$ionicLoading.show({
	            content: 'Loading',
	            animation: 'fade-in',
	            showBackdrop: true,
	            maxWidth: 200,
	            showDelay: 0
	        });

			$cordovaGeolocation
			.getCurrentPosition({enableHighAccuracy: true})
		    .then(function (position) {

		      	var currLatitude  = position.coords.latitude;
		      	var currLongitude = position.coords.longitude;

		      	$rootScope.sponsoredStartLatitude = currLatitude;
		      	$rootScope.sponsoredStartLongitude = currLongitude;

		      	var latlng = currLatitude+','+currLongitude;

		      	$rootScope.sponsoredCurrentAddres = latlng;

		      	HttpGetService.getSponsoredDetails(name)
			    .then(function(data) {

			    		if(data != "Error") {
			    			$rootScope.sponsoredAddress = data.entity.address;
				    		$rootScope.sponsoredName = data.entity.name;
				    		$rootScope.sponsoredEmail = data.entity.email;
				    		$rootScope.sponsoredPhone = data.entity.phone;
				    		$rootScope.sponsoredWorkHours = data.entity.work_hours;
				    		$rootScope.sponsoredImages = data.entity.images;
			    		}

			    		GetPlacesService.getCoordinates(address)
						.then(function(data) {

							    $ionicLoading.hide();
				      			$state.go('sponsoredmap', {sponsoredName: name, sponsoredLat: data.lat, sponsoredLng: data.lng, km: km, meters: meters});

						});

			    })


		    }, function(err) {
		      // error
		    });

    	}

    }
	
})

.controller('MapCtrl', function($scope, $rootScope, $timeout, $interval, $ionicNavBarDelegate, NgMap, $stateParams, $cordovaGeolocation, GetPlacesService, $cordovaLaunchNavigator, $ionicLoading, $localStorage, HttpGetService, $cordovaInAppBrowser) {
	
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.center = $stateParams.latitude+','+$stateParams.longitude;

	$scope.address = $stateParams.vicinity;

	$scope.distance = $stateParams.distance;

	var placeId = $stateParams.placeId;

	$scope.ratingshow = false;
	$scope.phoneshow = false;
	$scope.websiteshow = false;
	$scope.timeshow = false;

	GetPlacesService.getPlaceDetails(placeId)
	.then(function(data) {

		$scope.name= data.name;

		if(data.rating == null || data.rating == undefined || data.rating == "") {
			$scope.ratingshow = false;
		} else {
			$scope.rating = data.rating;
			$scope.ratingshow = true;
		}

		if(data.website == null || data.website == undefined || data.website == "") {
			$scope.websiteshow = false;
		} else {
			$scope.website = data.website;
			$scope.websiteshow = true;
		}

		if(data.international_phone_number == null || data.international_phone_number == undefined || data.international_phone_number == "") {
			$scope.phoneshow = false;
		} else {
			$scope.phone_number = data.international_phone_number;
			$scope.phoneshow = true;
		}

		if(data.opening_hours == null || data.opening_hours == undefined || data.opening_hours == "") {
			$scope.timeshow = false;
		} else {
			$scope.open = data.opening_hours.periods[0].open.time.substring(0, 2) + ':' + data.opening_hours.periods[0].open.time.substring(2, 4);
			$scope.close = data.opening_hours.periods[0].close.time.substring(0, 2) + ':' + data.opening_hours.periods[0].close.time.substring(2, 4);
			$scope.timeshow = true;
		}

	});

	$scope.call = function() {
		if($scope.phoneshow == false) {
			$ionicLoading.show({ templateUrl: 'templates/popups/phone.html', noBackdrop: true, duration: 1000 });
		} else {
			window.open('tel: ' + $scope.phone_number);
		}
	}

	NgMap.getMap().then(function(map) {
		$scope.zoom = function() {
			map.setZoom(map.getZoom() + 1);
		}

  	});

  	$scope.googleMapGo = function() {

		var destination = [$stateParams.latitude, $stateParams.longitude];
		var start = [$rootScope.startLatitude, $rootScope.startLongitude];

	    $cordovaLaunchNavigator.navigate(destination, start).then(function() {
	    }, function (err) {
	      console.error(err);
	    });
	    
  	}

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}

    $scope.goWebsite = function() {
        $cordovaInAppBrowser.open($scope.website, "_system");
    }
})

.controller('SponsoredMapCtrl', function($scope, $rootScope, NgMap, $stateParams, $timeout, $ionicNavBarDelegate, $cordovaGeolocation, GetPlacesService, $cordovaLaunchNavigator, $ionicModal, $ionicSlideBoxDelegate, $localStorage, HttpGetService, $ionicLoading, HttpGetService, $interval, $cordovaInAppBrowser) {
	
	$scope.$on('$ionicView.enter', function()
	{
	    $timeout(function()
	    {
	        $ionicNavBarDelegate.align('center');
	    });
	});

	$scope.phoneshow = false;
	$scope.emailshow = false;

	if($scope.sponsoredPhone == null || $scope.sponsoredPhone == undefined || $scope.sponsoredPhone == "") {
		$scope.phoneshow = false;
	} else {
		$scope.phoneshow = true;
	}

	if($scope.sponsoredEmail == null || $scope.sponsoredEmail == undefined || $scope.sponsoredEmail == "") {
		$scope.emailshow = false;
	} else {
		$scope.emailshow = true;
	}


	$scope.sponsoredCenter = $stateParams.sponsoredLat+','+$stateParams.sponsoredLng;;
	var sponsoredName = $stateParams.sponsoredName;
	$scope.sponsoredKm = $stateParams.km;
	$scope.sponsoredMeters = $stateParams.meters;


	NgMap.getMap().then(function(map) {
		$scope.zoom = function() {
			map.setZoom(map.getZoom() + 1);
		}
  	});

  	$scope.call = function() {

		if($scope.phoneshow == false) {
			$ionicLoading.show({ templateUrl: 'templates/popups/phone.html', noBackdrop: true, duration: 1000 });
		} else {
			window.open('tel: ' + $scope.sponsoredPhone);
		}
	}
  
    $ionicModal.fromTemplateUrl('templates/popups/image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      backdropClickToClose: false
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $ionicSlideBoxDelegate.slide(0);
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    $scope.$on('modal.hide', function() {
    });
    $scope.$on('modal.removed', function() {
    });
    $scope.$on('modal.shown', function() {
      
    });

    $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };
  
    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };
  
  	$scope.goToSlide = function(index) {
      $scope.modal.show();
      $ionicSlideBoxDelegate.slide(index);
    }
  
    $scope.slideChanged = function(index) {
      $scope.slideIndex = index;
    };

  	$scope.googleMapGo = function() {

		var destination = [$stateParams.sponsoredLat, $stateParams.sponsoredLng];
		var start = [$scope.sponsoredStartLatitude, $scope.sponsoredStartLongitude];

	    $cordovaLaunchNavigator.navigate(destination, start).then(function() {
	    }, function (err) {
	      	console.error(err);
	    });

  	}

	$scope.closeAd = function() {
		$interval.cancel($rootScope.interval);
		$scope.footerReklama = "footer-reklama-hide";
	}

	$scope.goAds = function() {
		$cordovaInAppBrowser.open($scope.ad.link, "_system");
	}


});