angular.module('placebook.services', ['pascalprecht.translate'])

    .service('HttpGetService', ['$http', '$ionicPopup', function ($http, $ionicPopup) {

        var token = '$2y$10$YjNjKBzzBej8d6m14ptsUOSlnyXIBxYVHKAgJ/UW6vJcJZvKI843C';
        var endpoint = 'http://placebook.mediarevolution.rs/api/v1/';

        function getSponsoredList(categorie, lat, long) {
            return $http.get(endpoint + 'groups/' + categorie + '/features?latitude=' + lat + '&longitude=' + long,
                {
                    headers: {"Authorization": "Bearer " + token}
                })
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    }
                }, function (response) {
                    // var alertPopup = $ionicPopup.alert({
                    //     title: response.status,
                    //     template: response.statusText,
                    // });
                    return "Error";
                })
        }

        function getSponsoredDetails(name) {
            return $http.get(endpoint + 'features/' + name,
                {
                    headers: {"Authorization": "Bearer " + token}
                })
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    }
                }, function (response) {
                    return "Error";
                })
        }

        function getFirstAd(lat, long) {
            return $http.get(endpoint + 'ads?first=1&latitude=' + lat + '&longitude=' + long,
                {
                    headers: {"Authorization": "Bearer " + token}
                })
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    }
                }, function (response) {
                    return "Error";
                })
        }

        function getRandomAd(lat, long, name) {
            return $http.get(endpoint + "ads?name=" + name + "&latitude=" + lat + "&longitude=" + long,
                {
                    headers: {"Authorization": "Bearer " + token}
                })
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    }
                }, function (response) {
                    return "Error";
                })
        }

        return {
            getSponsoredList: getSponsoredList,
            getSponsoredDetails: getSponsoredDetails,
            getFirstAd: getFirstAd,
            getRandomAd: getRandomAd
        }

    }])

    .service('GetPlacesService', ['$http', '$q', '$ionicPopup', function ($http, $q, $ionicPopup) {
        // var key = 'AIzaSyAKfX6LiXd56wqoYTiI_qtsub_tCU93NMs';
        var key = 'AIzaSyCzZgoeSCdsiUc-JV9JGj6NyBDLSNlHXoU';

        function getPlaces(location, type, keyword) {
            var dfd = $q.defer();

            getList(location, type, keyword).then(function (data) {

                var newData = data;

                dfd.resolve(newData);
            })

            return dfd.promise;
        }

        function getDistance(newData, lat, lng) {
            for (var i = 0; i < newData.length; i++) {
                newData[i].distance = getDistanceFromLatLonInKm(lat, lng, newData[i].geometry.location.lat, newData[i].geometry.location.lng);
            }

            function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
                var R = 6371; // Radius of the earth in km
                var dLat = deg2rad(lat2 - lat1);  // deg2rad below
                var dLon = deg2rad(lon2 - lon1);
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = R * c; // Distance in km
                return d;
            }

            function deg2rad(deg) {
                return deg * (Math.PI / 180)
            }

            newData.sort(function (a, b) {
                if (a.distance < b.distance) return -1;
                if (a.distance > b.distance) return 1;
                return 0;
            });

            angular.forEach(newData, function (key, value) {
                if (key.distance < 1) {
                    key.distance = key.distance * 1000;
                    key.distance = key.distance.toFixed();
                    key.distance = key.distance + " m";
                } else {
                    key.distance = key.distance.toFixed(1);
                    key.distance = key.distance + " km";
                }
            });

            return newData;

        }

        function getInfo(newData, i) {
            return getPlaceDetails(newData[i].place_id)
                .then(function (data) {
                    newData[i].info = data;
                })
        }

        function getList(location, type, keyword) {
            var type = type;
            var currLocation = location;
            var keyword = keyword;
            var dfd = $q.defer();

            $http.get('https://maps.googleapis.com/maps/api/place/radarsearch/json?location=' + currLocation + '&language=en&radius=50000&type=' + type + '&keyword=' + keyword + '&key=' + key)
                .success(function (response) {
                    var data = response.results;

                    if (response.status != "OK" && response.status != "ZERO_RESULTS") {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Google API Limit',
                            template: 'You exceeded your daily limit for Google API Request.',
                        });
                    }

                    dfd.resolve(data);
                });

            return dfd.promise;
        }

        function getPlaceDetails(placeId) {
            var dfd = $q.defer();
            $http.get('https://maps.googleapis.com/maps/api/place/details/json?placeid=' + placeId + '&key=' + key)
                .success(function (data) {
                    dfd.resolve(data.result);
                });
            return dfd.promise;
        }

        function getCurrentAddress(location) {
            var currLocation = location;

            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + currLocation + '&sensor=true&key=' + key)
                .then(function (response) {
                    return response.data.results[0].formatted_address;
                })
        }

        function getLocations(input) {
            return $http.get('https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + input + '&types=geocode&key=' + key)
                .then(function (response) {
                    return response.data;
                })
        }

        function getCoordinates(address) {
            return $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&types=geocode&key=' + key)
                .then(function (response) {
                    if (response.data.results.length != 0) {
                        return response.data.results[0].geometry.location;
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Wrong Address',
                            template: "The address for sponsored object doesn't exist"
                        });
                    }

                })
        }

        return {
            getPlaces: getPlaces,
            getPlaceDetails: getPlaceDetails,
            getCurrentAddress: getCurrentAddress,
            getInfo: getInfo,
            getDistance: getDistance,
            getLocations: getLocations,
            getCoordinates: getCoordinates
        }

    }])

    .service('WeatherService', ['$http', '$ionicPopup', '$localStorage', function ($http, $ionicPopup, $localStorage) {

        function getWeather(lat, lng) {
            return $http.get("http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&units=" + $localStorage.units + "&APPID=640f38d514a9dfed10a4477efc8bb42a")
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Google API Limit',
                            template: 'You exceeded your daily limit for Google API Request.',
                        });
                    }
                })
        }

        function getForecast(lat, lng) {
            return $http.get("http://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lng + "&units=" + $localStorage.units + "&APPID=640f38d514a9dfed10a4477efc8bb42a")
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data.list;
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Google API Limit',
                            template: 'You exceeded your daily limit for Google API Request.',
                        });
                    }
                })
        }

        function getWeatherChangedLocation(location) {
            return $http.get("http://api.openweathermap.org/data/2.5/weather?q=" + location + "&units=" + $localStorage.units + "&APPID=640f38d514a9dfed10a4477efc8bb42a")
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data;
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Google API Limit',
                            template: 'You exceeded your daily limit for Google API Request.',
                        });
                    }
                })
        }

        function getForecastChangedLocation(location) {
            return $http.get("http://api.openweathermap.org/data/2.5/forecast?q=" + location + "&units=" + $localStorage.units + "&APPID=640f38d514a9dfed10a4477efc8bb42a")
                .then(function (response) {
                    if (response.status == 200) {
                        return response.data.list;
                    } else {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Google API Limit',
                            template: 'You exceeded your daily limit for Google API Request.',
                        });
                    }
                })
        }

        return {
            getWeather: getWeather,
            getForecast: getForecast,
            getWeatherChangedLocation: getWeatherChangedLocation,
            getForecastChangedLocation: getForecastChangedLocation
        }
    }]);